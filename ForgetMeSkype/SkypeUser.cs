﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForgetMeSkype
{
    public class SkypeUser
    {
        public string Username { get; set; }
        public string Directory { get; set; }

        public SkypeUser(string username, string directory)
        {
            Username = username;
            Directory = directory;
        }
    }
}
