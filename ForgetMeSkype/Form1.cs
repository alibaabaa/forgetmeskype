﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ForgetMeSkype
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            UserComboBox.DisplayMember = "Username";
            UserComboBox.ValueMember = "Directory";
            PopulateUserList();
        }

        private void PopulateUserList()
        {
            UserComboBox.SelectedItem = null;
            UserComboBox.Items.Clear();
            UserComboBox.Items.AddRange(UsernamesFound().ToArray());
            RemoveButton.Enabled = UserComboBox.Items.Count > 0;
            if (UserComboBox.Items.Count > 0)
            {
                UserComboBox.SelectedIndex = 0;
            }
        }

        private void RemoveButtonClick(object sender, EventArgs e)
        {
            var selectedUser = (UserComboBox.SelectedItem as SkypeUser);
            if (selectedUser != null)
            {
                Directory.Delete(selectedUser.Directory, true);
            }
            PopulateUserList();
        }

        private IEnumerable<SkypeUser> UsernamesFound()
        {
            foreach (var directory in new DirectoryInfo(SkypeDirectory).GetDirectories())
            {
                if (File.Exists(directory.FullName + @"\config.xml"))
                {
                    yield return new SkypeUser(directory.Name, directory.FullName);
                }
            }
        }

        private string SkypeDirectory
        {
            get
            {
                return string.Format(
                    @"{0}\Skype",
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            }
        }
    }
}
